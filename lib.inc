section .text


; Принимает код возврата и завершает текущий процесс
exit:
         mov rax, 60
         xor rdi, rdi
         syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
        xor rax, rax
        mov rax, -1
       .count:
        inc rax
        cmp byte[rdi + rax], 0
        jne .count
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
        push rdi
        call string_length
        pop rsi
        mov rdx, rax
        mov rax, 1
        mov rdi, 1
        syscall
        ret


; Принимает код символа и выводит его в stdout
print_char:
        push rdi
        mov rdi, 1
        mov rax, 1
        mov rdx, 1
        mov rsi, rsp
        syscall
        pop rsi
        ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
        mov rax, 1
        mov rdi, 1
        mov rsi, 0xA
        mov rdx, 1
        syscall
        ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
        mov rcx, 8
        mov rax, rdi
        mov rsi, 0xA
        mov r8, 8

        push 0x0
        .loop:
        xor rdx, rdx
        div rsi
        add rdx, 48
        dec rsp
        mov byte[rsp], dl
        inc rcx
        test rax, rax
        jne .loop

        .stack_line:
        mov rax, rcx
        mov rdi, rsp
        xor rdx, rdx
        div r8
        add rcx, rdx
        sub rsp, rdx
        jmp .close

        .close:
        push rcx
        call print_string
        pop rcx
        add rsp, rcx
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
        test rdi, rdi
        jns print_uint
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        jmp print_uint
        ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
        .check_length:
        push rdi ; заносим в стек rdi
        push rsi ; заносим в стек rsi
        call string_length ; вызов функции string_length

        mov r12, rax ; записываем аккумулятор в r12
        pop rsi ; записываем последнее значение со стека в регистр rsi
        mov rdi, rsi
        push rsi
        call string_length
        pop rsi
        pop rdi
        cmp rax, r12
        jne .non_equals
    xor rax, rax
    .loop:
        test r12, r12
        je .equals
        mov r9b, byte[rsi + rax]
        cmp byte[rdi + rax], r9b
        jne .non_equals
        inc rax
        dec r12
        jmp .loop
    .non_equals:
        mov r12, 0
        xor rax, rax
        ret
    .equals:
        mov rax, 1
        ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        xor rdi, rdi
        xor rax, rax
        push 0
        mov rdx, 1
        mov rsi, rsp
        syscall
        pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
         xor rdx, rdx
    .read_word:
         push rsi
        push rdx
        push rdi

        call read_char
        pop rdi
        pop rdx
        pop rsi
        cmp rax, ' '
        je .read_word
        cmp rax, 9
        je .read_word
        cmp rax, 0xA
        je .read_word
    .read:
        cmp rsi, rdx
        jb .mistake
        cmp rax, 0x0
        je .close
        cmp rax, ' '
        je .mistake
        mov byte[rdi + rdx], al
        inc rdx
        push rsi
        push rdx
        push rdi
        call read_char
        pop rdi
        pop rdx
        pop rsi
        jmp .read
    .close:
        mov [rdi + rdx],  byte 0
        mov rax, rdi
        ret
    .mistake:
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        xor rax, rax
        xor r9, r9
        mov r10, 0xA
        xor rcx, rcx

        .loop:
                mov r9b, byte[rdi+rcx]
                xor r9b, '0'
                cmp r9b, 9
                ja .end
                mul r10
                add al, r9b
                inc rcx
        jmp .loop
        .end:
                mov rdx, rcx
                ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
        mov r9b, byte[rdi]
        cmp r9b, '-'
        jne parse_uint
        inc rdi
        call parse_uint
        test rdx, rdx
        je .end
        neg rax
        inc rdx
        .end:
                ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
        push rsi
        push rdx
        push rdi
        call string_length
        pop rdi
        pop rdx
        pop rsi
        inc rax
        cmp rdx, rax
        jb .mistake
        mov r10, rax
        xor rcx, rcx
  .loop:
        mov r9b, byte[rdi + rcx]
        mov byte[rsi + rcx], r9b
        inc rcx
        dec r10
        test r10, r10
        jne .loop
  .end:
        ret
  .mistake:
        xor rax, rax
         ret
